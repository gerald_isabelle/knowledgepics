module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            dist: {
                files: {
                    'public/css/styles.css': 'public/scss/styles.scss',
                }
            }
        },

        cssmin: {
            compress: {
                files: {
                    'public/css/styles.min.css': 'public/css/styles.css'
                }
            },
        },

        watch: {
            default: {
                files: ['public/scss/**/*', 'public/script/**/*.js'],
                tasks: ['sass', 'cssmin','concat', 'uglify'],
                options: {
                    livereload: true,
                }
            }
        },

        concat: {
          options: {
            // define a string to put between each file in the concatenated output
            separator: ';'
          },
          dist: {
            // the files to concatenate
            src: ['public/script/vendor/*.js',
                  'public/script/bootstrap.js',
                  'public/script/app.js',
                  'public/script/directives/*.js',
                  'public/script/controllers/*.js',
                  'public/script/services/*.js',
                  'public/script/bootstrap/carousel.js',
                  'public/script/bootstrap/transition.js',
                  'public/script/bootstrap/collapse.js',
                  'public/script/bootstrap/dropdown.js',
                  'public/script/bootstrap/modal.js'
                  ],
            // the location of the resulting JS file
            dest: 'public/js/script.js'
          }
        },

        uglify: {
          dist: {
            files: {
              'public/js/script.min.js': ['public/js/script.js']
            }
          }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['watch']);

};
