<?php include 'global/header.php'; ?>
<?php  include('global/navigation.php') ?>
<?php include("cms/functions.inc"); ?>

<?php if (!$_SESSION["username"]) { ?>

<?php $books=getBooks($_SESSION["username"]); ?>

<div class="light-gray-background">
  <div class="container text-center">
    <h1>learn The Blue Books</h1>
  </div>
</div>
<div class="container">
  <div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book1.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 1 list 1-5</h3>
      <p>Book 1 is <strong class="text--red">FREE !</strong></p>
	  <?php if(!$_SESSION["username"]){ ?>
      <p class="text--primary">You will only need to sign up and subscribe to view <strong>List 3-5</strong></p>
	   <a class="btn gray-background text--norwester float--right hard--ends" href="<?php echo helper::host() ?>users/register.php">Log In Now</a>
	   </br>
      <a class="btn gray-background text--norwester float--right hard--ends" style="margin-right:-95px; margin-top:11px" href="<?php echo helper::host() ?>users/register.php">Sign up Now</a>
	  <?php }else { ?>

			<p class="text--primary"><b>You are already SINGED! You can browse Book 1 for free.</b></p>

	  <?php } ?>

    </div>
  </div>
<?php


$key = array_search('2', $books);

if(count($books)==0 && !$key){

?>
  <div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book2.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 2 list 6-10</h3>
      <table>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>7 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£7.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">
		  		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 2 list 6-10, 7 days"><input type="hidden" name="amount_1" value="7.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.60">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="8.59">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>
		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>21 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£12.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">
		   <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 2 list 6-10, 21 days"><input type="hidden" name="amount_1" value="12.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.7">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="13.69">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>
		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>28 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£14.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">

		   <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 2 list 6-10, 28 days"><input type="hidden" name="amount_1" value="14.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.8">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="15.79">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>
		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>56 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£21.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">
		    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 2 list 6-10, 56 days"><input type="hidden" name="amount_1" value="21.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="1">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="22.99">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>


		  </td>
        </tr>
      </table>

      <p class="soft-half--top">Access to Runs and over 2000 picture points from Book 2</p>

    </div>
  </div>

<?php
		}else{
?>

<div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book2.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 2 list 6-10</h3>
	</div>
	<div style="margin-left:280px">
      <p class="soft-half--top">You have subscribed for Book 2. Your details are: </p>

	  	<?php
						   $results=getBookOrder($_SESSION["username"], 1);
						?>
						<table style="margin-left:10px" border="1" >
  <tr>
	<th> Email </th>
    <th> Subscription </th>
    <th> End Date</th>
    <th> Book </th>
	<th> Price </th>
  </tr>
  <?php
		while($result=mysql_fetch_assoc($results)){
  ?>
  <tr>
    <td align="center" ><?php echo $result['email']?></td>
	<td align="center"><?php echo date('d-m-Y', strtotime($result['startDate'])); ?></td>
    <td align="center"><?php echo date('d-m-Y', strtotime($result['endDate'])); ?></td>
    <td align="center"><?php echo ($result['book']+1)?></td>
    <td align="center">&pound<?php echo $result['price']?></td>
  </tr>
  <? } ?>
</table>
</div>

</div>

<?php
		}
?>

<?php

$key = array_search('3', $books);


if(!$key){

?>

  <div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book3.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 3 list 11-15</h3>
      <table>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>7 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£7.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">
		  		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 3 list 11-15, 7 days"><input type="hidden" name="amount_1" value="7.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.6">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="8.59">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>


		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>21 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£12.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">

		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 3 list 11-15, 21 days"><input type="hidden" name="amount_1" value="12.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.7">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="13.69">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>

		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>28 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£14.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">
		  <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 3 list 11-15, 28 days"><input type="hidden" name="amount_1" value="14.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="0.8">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="15.79">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>
		  </td>
        </tr>
        <tr>
          <td class="col-xs-1 hard--sides"><strong>56 days</strong></td>
          <td class="col-xs-2 hard--left">subscription</td>
          <td class="col-xs-2"></td>
          <td class="col-xs-1 hard--sides">price:</td>
          <td class="col-xs-1 hard--left"><strong>£21.99</strong></td>
          <td class="col-xs-2"></td>
          <td class="col-xs-2 hard--right">

		   <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_cart">
<input type="hidden" name="upload" value="1">
<input type="hidden" name="business" value="admin@knowledgepics.com">
<input type="hidden" name="currency_code" value="GBP">

                <input type="hidden" name="item_name_1" value="Book 3 list 11-15, 56 days"><input type="hidden" name="amount_1" value="21.99">
				<input type="hidden" name="quantity_1" value="1"><input type="hidden" name="shipping_1" value="">
				<input type="hidden" name="item_name_2" value="PayPal Fees">
				<input type="hidden" name="amount_2" value="1">
				<input type="hidden" name="quantity_2" value="1">
				<input type="hidden" name="shipping_2" value="0">
				<input type="hidden" name="total" value="22.99">
				<input type="hidden" name="ways" value="3">
				<input type="hidden" name="tax_cart" value="0">

<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
			 <input type="submit" style="margin-left:-10px" name="submit" class="btn gray-background text--norwester float--right hard--ends" value="Subscribe now!">

	</form>
		  </td>
        </tr>
      </table>

      <p class="soft-half--top">Access to Runs and over 2000 picture points from Book 3</p>
    </div>
  </div>


  <?php
		}else{
?>

<div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book2.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 3 list 11-15</h3>
	</div>
	<div style="margin-left:280px">
      <p class="soft-half--top">You have subscribed for Book 3. Your details are: </p>

	  	<?php
						   $results=getBookOrder($_SESSION["username"], 2);
						?>
						<table style="margin-left:10px" border="1" >
  <tr>
	<th> Email </th>
    <th> Subscription </th>
    <th> End Date</th>
    <th> Book </th>
	<th> Price </th>
  </tr>
  <?php
		while($result=mysql_fetch_assoc($results)){
  ?>
  <tr>
    <td align="center" ><?php echo $result['email']?></td>
	<td align="center"><?php echo date('d-m-Y', strtotime($result['startDate'])); ?></td>
    <td align="center"><?php echo date('d-m-Y', strtotime($result['endDate'])); ?></td>
    <td align="center"><?php echo ($result['book']+1)?></td>
    <td align="center">&pound<?php echo $result['price']?></td>
  </tr>
  <? } ?>
</table>
</div>

</div>

<?php
		}
?>
  <div class="row push--top border--grey push--sides">
    <div class="col-xs-3 hard">
      <img src="<?php echo helper::host() ?>img/books/book4.jpg" alt="">
    </div>
    <div class="col-xs-9 soft--left">
      <h3 class="text--primary text--norwester">Book 4</h3>
      <p class="text--primary">Book 4 is currently not available.</p>
    </div>
  </div>
</div>



<?php include 'global/footer.php'; ?>

<?php
  }
  else{
    $next_page = helper::host()."map.php?run_id=5&list_id=1&book=1";
    header("Location: $next_page");
  }


?>
