<?php

/* Program: logout.php
* Desc: user logout.
*/

session_start();

if (@$_SESSION["username"] != null)
{
   $_SESSION["username"] = null;
   $next_page = "login.php";
   header("Location: $next_page");       
}

?>