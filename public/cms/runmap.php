<?php

include("functions.inc");

?>

<!DOCTYPE html>
<html> 
<head> 
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
  <title>Google Maps Multiple Markers</title> 
  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
</head> 
<body>

<div style="font-weight:bold;">A review of all points | <a href="menu.php"> Back to the Menu </a> </div>

  <div id="map" style="width: 1400px; height: 800px;"></div>

  <script type="text/javascript">

    var locations = [

<?php  $maps=getmaprelations($_GET["id"]); $i=0;
while($map=@mysql_fetch_assoc($maps))
   { $i++;

$videos=getindiv($map['id']);
$video= @mysql_fetch_assoc($videos);

$html='<a href="updateind.php?id='.$map['id'].'">'.addslashes($video['title']).'</a>';

?>
  ['<b><?php  echo $html; ?></b></br>',<?php echo $map['lang'];?> ,<?php echo $map['lng'];?> , <?php echo $i; ?>],

<?php } ?>

    ];
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: new google.maps.LatLng(51.511307,-0.121536),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
          console.log('Position: '+marker.position);
        }
      })(marker, i));
    }
  </script>

<script type="text/javascript">
//<![CDATA[

rad = function(x) {return x*Math.PI/180;}

function distHaversine(p1, p2) {
  var R = 6371; // earth's mean radius in km
  var dLat  = rad(p2.lat() - p1.lat());
  var dLong = rad(p2.lng() - p1.lng());

  var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) * Math.sin(dLong/2) * Math.sin(dLong/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var d = R * c;

  return d.toFixed(3);
}
</script>

</body>
</html>
