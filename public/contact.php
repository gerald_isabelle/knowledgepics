<?php
    error_reporting(0);
    session_start();
    $javascript_enabled = "";
    $errorMsg = "";
    $name ="";
    $email = "";
    $phone = "";
    $message = "";
    $verifyImage= "";
    $security= "";

    if (($_POST["userAction"] == "sendForm")){

       $javascript_enabled = trim($_REQUEST['browser_check']);
         $name = $_POST['name'];
         $email = $_POST['email'];
         $phone = $_POST['phone'];
         $message = $_POST['message'];
       $verifyImage=$_POST['verif_box'];
       $security=$_SESSION['security_code'];


      if((!$name)||(!$email)||(!$message)||(!$verifyImage)){

        // check if the required fields are not emtpy

           $errorMsg = 'Can you please fill in the following required information:<br /><br />';

             if(!$name){
               $errorMsg .= '<div class="error">* Name</div><br />';
               }
           if(!$email){
               $errorMsg .= '<div class="error">* Email</div><br />';
               }
             if(!$message ){
               $errorMsg .= '<div class="error">* Message</div><br />';
              }
             if(!$verifyImage) {
             $errorMsg .= '<div class="error">* image verification</div><br />';
              }
          }
          // check if email is valid
          else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){
               $errorMsg .= '<div class="error">* A valid email</div><br />';
               }
          // check if image verification match input
           else if($verifyImage != $security ){
             $errorMsg .= '<div class="error">* Please enter the correct image verification</div><br />';
           }


          else {
               // Error handling is ended, process the data and add member to database

            $to = "admin@knowledgepics.com";
            $from = "admin@knowledgepics.com";
            $subject = "knowledgepics Contact Form";
            $content = '<html>
                    <body bgcolor="#FFFFFF">
                      <b>From knowledgepics Contact Form</b>,<br/><br/>
                        <b>Name</b> : '.$name.' <br>
                        <b>Email Address</b> : '.$email.' <br>
                        <b>Phone</b> : '.$phone.' <br>
                        <b>Message</b> : '.$message.'



                    </body>
                  </html>';
                $headers = "From: $from\r\n";
                $headers .= "Content-type: text/html\r\n";
                $to = "$to";

                if(mail($to, $subject, $content, $headers)){
      // Then print a message to the browser for the joiner
                   $errorMsg="<p align='center'>Thank you very much for your inquiry. We will try to respond within 24 hours.</p>";
                  }else{
                   $errorMsg="<h3 style='text-align:center;color:#000;border:1px solid red;'>Unfortunately their was a techincal problem while sending your request. Pleasy try again or phone us on 0208 847 8000. Thank you.</h3>";
                  }

              }

              if($javascript_enabled == "true") {
                echo $errorMsg;
                die();
              }
            }
          else { // if the form is not posted with variables, place default empty variables

              $errorMsg = "";
              $name ="";
              $email = "";
              $phone = "";
              $message = "";
              $verifyImage= "";
              $security= "";

    }

?>
<?php include 'global/header.php'; ?>

<?php include 'global/navigation.php'; ?>

<div class="light-gray-background">
  <div class="container"><h1>Contact Us</h1></div>
</div>

<div class="lighter-gray-background soft--top">
  <div class="container soft--ends">

    <div class="row">
      <div class="col-xs-7">
        <p class="contact"> Please fill in the form below. <u> All data except telephone number is mandatory</u> <span class="required">*</span>. Add as much details as you can so we can promptly reply to your specific needs. Thank you.</p>
        <div id="result"></div>
        <div id="response"></div>
        <form name="contactform" class="form-signin" id="form" >
          <input name="userAction" value="sendForm" type="hidden">
          <input type="hidden" name="browser_check" value="false" />
            <div class="fieldwrapper">
                    <label for="username" class="styled">Your Name: <span class="required">*</span></label>
                    <div class="thefield">
                       <input name="name" type="text" id="name" class="form-control"  value="">
                    </div>
                </div>
                <div class="fieldwrapper">
                    <label for="email" class="styled">Email address: <span class="required">*</span></label>
                    <div class="thefield">
                        <input name="email" type="text" class="form-control" id="email" value="" >
                    </div>
                </div>
                <div class="fieldwrapper">
                    <label for="email" class="styled">Telephone Number:</label>
                    <div class="thefield">
                        <input name="phone" type="text" id="telephone" class="form-control" value="" >
                    </div>
                </div>
                 <div class="fieldwrapper">
                    <label for="about" class="styled">Message: <span class="required">*</span></label>
                      <div class="thefield">
                      <textarea name="message" cols="35" rows="4" id="about" class="form-control" value=""></textarea>
                </div>
                </div>
                <div class="fieldwrapper">
                    <label for="about" class="styled">Type image verification: <span class="required">*</span></label>
                      <div class="thefield">
                          <input name="verif_box" type="text" value="" class="form-control"  />
                  </div>
                  <div style="clear:both; padding-top:20px;"><img src="<?php echo helper::host() ?>lib/CaptchaSecurityImages.php?width=100&height=25&characters=5" /><br /></div>
                </div>
                <div class="buttonsdiv">
                    <button type="submit" class="btn btn-lg btn-primary btn-block"  name="submit" id="submit">Send</button>
                </div>
             </form><br />
          or Email us at <a href= "mailto:admin@knowlegdepics.com" >admin@knowlegdepics.com</a>
        </div>
      <div class="col-xs-5">
          <p>For any inquiries please fill in the form leaving your email and we well contact you as soon as possible. You will normally get a reply within 48 hours after inquiry submission.</p>
          <h4>Privacy</h4>
          <p>Any personal information used by Knowledgepics.Com in the course of records and transactions will never be passed to any third party without the expressed permission of the person concerned.</p>
        </div>

    </div>

  </div>
</div>


<?php include 'global/footer.php'; ?>
