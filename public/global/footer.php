<footer>
  <div class="blue-background push-half--top soft--top soft--bottom">
    <div class="container push--bottom">
      <div class="row">
        <div class="col-xs-4">
          <h3 class="flush--top text--white soft--ends">Knowledgepics</h3>
          <div class="col-xs-6 hard--left">
            <ul class="footer-list hard--left">
              <li class="soft-half--bottom"><a href="<?php echo helper::host() ?>books.php" class="text--white soft--bottom" title="blue books">Blue Books</a></li>
              <li><a href="<?php echo helper::host() ?>terms.php" class="text--white" title="Terms">Terms</a></li>
            </ul>
          </div>
          <div class="col-xs-6">
            <ul class="footer-list hard--left">
              <li class="soft-half--bottom"><a href="<?php echo helper::host() ?>users/register.php" class="text--white" title="sign up">Sign up</a></li>
              <li><a href="<?php echo helper::host() ?>users/login.php" class="text--white" title="login">Login</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-8 soft--left">
          <div class="soft--left">
          <h3 class="flush--top text--white soft--ends">Contact us</h3>
            <ul class="footer-list hard--left">
              <li class="soft-half--bottom"><a href="maito:info@knowledgepics.com" class="text--white">Info@knowledgepics.com</a></li>
              <li class="soft-half--bottom"><a href="maito:technicalsupport@knowledgepics.com" class="text--white">Technicalsupport@knowledgepics.com</a></li>
              <li class="soft-half--bottom"><a href="maito:enquires@knowledgepics.com" class="text--white">Enquires@knowledgepics.com</a></li>
            </ul>
            <a href="https://twitter.com/Knowledgepics" class="push-half--right text--white" title="twitter"><img alt="facebook" src="http://www.knowledgepics.com/img/icons/twitter_white.svg" width=34px"></a>
            <a href="" class="text--white" title="facebook"><img alt="facebook" src="http://www.knowledgepics.com/img/icons/facebook_white.svg" class="text--white" width="34px"></span></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="footer">
    <div class="container marketing text--right soft-half--ends">
      <p>&copy; Knowledgepics.com, <?php echo date("Y"); ?></p>
    </div>
  </div>
</footer>


<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-3984654-11', 'auto');
ga('send', 'pageview');

</script>

<script src="<?php echo helper::host() ?>js/script.min.js"></script>


</body>
</html>
