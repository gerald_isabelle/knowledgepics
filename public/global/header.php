<?php session_start(); ?>

<?php include 'settings.php'; ?>

<!DOCTYPE html>
<html data-ng-app="knowledgeApp">
<head>
<!-- TradeDoubler site verification 2415425  -->
    <meta charset="utf-8">

    <?php include 'metatags.php'; ?>


    <!-- Add favicon -->
    <link rel="icon"
      type="image/png"
      href="/favicon.png?=0">


    <link href="http://www.knowledgepics.com/css/styles.css" rel="stylesheet">
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>


</head>
<body>
