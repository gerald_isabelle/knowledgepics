<div class="navbar-wrapper navbar-wrapper--no-border blue-background">
    <div class="container header">
        <div class="navbar-header soft-half--ends flush--sides">
            <a href="http://www.knowledgepics.com/" class="navbar-brand hard--left" title="home"><img src="http://www.knowledgepics.com/img/logo_white.png" alt="logo"></a>
        </div>
        <ul class="nav nav-pills map-book">
            <li class="text--white push-half--top push-half--right">Books</li>
            <li class="map-book--list <?php echo ($_GET['book'] == 1) ?  'map-book-active ' : '' ?> push-half--right"><a href="http://www.knowledgepics.com/map.php?run_id=5&list_id=1&book=1" class="hard flush">1</a></li>
            <li class="map-book--list <?php echo ($_GET['book'] == 2) ?  'map-book-active ' : '' ?> push-half--right"><a href="http://www.knowledgepics.com/map.php?run_id=88&list_id=87&book=2" class="hard flush">2</a></li>
            <li class="map-book--list <?php echo ($_GET['book'] == 3) ?  'map-book-active ' : '' ?> push-half--right"><a href="http://www.knowledgepics.com/map.php?run_id=174&list_id=172&book=3" class="hard flush">3</a></li>
            <li class="map-book--list map-book-disabled push-half--right"><a href="" class="hard flush link--disabled">4</a></li>
        </ul>

<?php if($_SESSION["username"]){ ?>

        <ul class="nav nav-pills login-details">
            <li class="text--white"><?php echo $_SESSION["username"]; ?></li>
        </ul>
        <div class="login">
            <ul>
                <li><a href="http://www.knowledgepics.com/users/logout.php" title="log out" class="push-half--right"><img src="http://www.knowledgepics.com/img/icons/logout.svg" width="34px" alt="log out"></a></li>
                <li><a href="http://www.knowledgepics.com/users/profile.php" title="my profile"><img src="http://www.knowledgepics.com/img/icons/profile.svg" width="34px" alt="my profile"></a></li>
            </ul>
        </div>
<?php } ?>

    </div>
</div>