<div class="navbar-wrapper blue-background">
    <div class="container header">
        <div class="navbar-header flush--sides">
            <a href="http://www.knowledgepics.com/" class="navbar-brand hard--left" title="home"><img src="http://www.knowledgepics.com/img/logo.png" alt="logo"></a>
        </div>
        <?php if (!(isset($_SESSION['username']) && $_SESSION['username'] != '')){ ?>
            <div class="social-icons">
                <a href="https://twitter.com/Knowledgepics" title="twitter" class="push-half--right"><img src="http://www.knowledgepics.com/img/icons/twitter.svg" width="34px" alt="twitter"></a>
                <a href="" title="facebook"><img src="http://www.knowledgepics.com/img/icons/facebook.svg" alt="facebook" width="34px"></span></a>
            </div>
            <div class="login">
                <ul>
                    <li><a class="btn-primary" href="http://www.knowledgepics.com/users/register.php" title="register">Sign up</a></li>
                    <li class="push--left"> <a class="btn-primary"  href="http://www.knowledgepics.com/users/login.php" title="log in">Log in</a></li>
                </ul>
            </div>
        <?php } else { ?>
            <ul class="nav nav-pills login-details">
                <li class="text--white"><?php echo $_SESSION["username"];?></li>
            </ul>
            <div class="login">
                <ul>
                    <li><a href="http://www.knowledgepics.com/users/logout.php" title="log out" class="push-half--right"><img src="http://www.knowledgepics.com/img/icons/logout.svg" width="34px" alt="log out"></a></li>
                    <li><a href="http://www.knowledgepics.com/users/profile.php" title="my profile"><img src="http://www.knowledgepics.com/img/icons/profile.svg" width="34px" alt="my profile"></a></li>
                </ul>
            </div>
        <?php } ?>
    </div>
</div>
<div class="navbar" role="navigation">
  <ul class="nav navbar-nav">
    <li><a href="http://www.knowledgepics.com/" title="home" <?php echo ($_SERVER['REQUEST_URI'] == '/' ? 'class="active"':'')?>>Home</a></li>
    <li><a href="http://www.knowledgepics.com/books.php" title="Books" <?php echo ($_SERVER['REQUEST_URI'] == '/books.php' ? 'class="active"':'')?>>Books</a></li>
    <li><a href="http://www.knowledgepics.com/video.php" title="Video" <?php echo ($_SERVER['REQUEST_URI'] == '/video.php' ? 'class="active"':'')?>>Video</a></li>
    <li><a href="http://www.knowledgepics.com/contact.php" title="contact" <?php echo ($_SERVER['REQUEST_URI'] == '/contact.php' ? 'class="active"':'')?>>contact</a></li>
  </ul>
</div>
