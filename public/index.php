﻿<?php include 'global/header.php'; ?>

<?php include 'global/navigation.php'; ?>
<?php include("cms/functions.inc"); ?>
<?php $books=getBooks($_SESSION["username"]);


?>
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item six active">
            <div class="container">
              <div class="soft text--center">
                <video id="video-intro" height="240" controls>
                  <source src="http://www.knowledgepics.com/video/londontaxi.webm" type="video/webm">
                  <source src="http://www.knowledgepics.com/video/londontaxi.mp4" type="video/mp4"
                </video>
              </div>
            </div>
        </div>
    </div>
    <div class="container books-section">
      <div class="row books-section__wrapper soft-half--left">
        <div class="col-xs-3 hard--left">
          <a href="map.php?run_id=5&list_id=1&book=1" title="book 1">
            <div class="books-section__container soft-half text--center">
              <img src="<?php echo helper::host() ?>img/books/book1.png" alt="book 1">
              <p class="books-section__list">Book 1 List 1-5</p>
              <p class="soft-half--ends books-section__description"><strong>Start learning the 320 runs to become a London Taxi Driver</strong></p>
            </div>
          </a>
        </div>
        <div class="col-xs-3 hard--left">
          <a href="map.php?book=2" title="book 2">
            <div class="books-section__container soft-half text--center">
              <img src="<?php echo helper::host() ?>img/books/book2.png" alt="book 2">
              <p class="books-section__list">Book 2 List 6-10</p>
              <p class="soft-half--ends books-section__description"><strong>80 runs in each Book</strong></p>
            </div>
          </a>
        </div>
        <div class="col-xs-3 hard--left">
          <a href="map.php?book=3" title="book 3">
            <div class="books-section__container soft-half text--center">
              <img src="<?php echo helper::host() ?>img/books/book3.png" alt="book 3">
              <p class="books-section__list">Book 3 List 11-15</p>
              <p class="soft-half--ends books-section__description"><strong>Thousands of Picture Points to help you locate &#38; revise</strong></p>
            </div>
          </a>
        </div>
        <div class="col-xs-3 hard--left">
          <a href="books.php" title="book 1">
            <div class="books-section__container soft-half text--center">
              <img src="<?php echo helper::host() ?>img/books/book4.png" alt="book 4">
              <p class="books-section__list">Book 4 List 16-20</p>
              <p class="soft-half--ends books-section__description"><strong>The books cover a six mile radius from Charing Cross Station WC2</strong></p>
            </div>
          </a>
        </div>
      </div>
    </div>
</div>


<div class="container-border-red">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h1 class="item-header text--center">Sign up for full access to a FREE trial of KnowledgePics</h1>
      </div>
    </div>
  </div>
</div>

<div class="soft--top soft--bottom container-border-grey">
  <div class="container push--bottom">
    <div class="row">
      <div class="col-xs-8">
        <h3 class="soft--bottom text--primary">Created specially for The Knowledge</h3>

          <p>Knowledgepics helps individuals to revise and learn The Blue Book to become an All London Taxi Driver.</p>
          <p>As the brain remembers image more effectively than words we associate all of our ‘points’ with pictures helping you to memorize points easier.</p>
          <p>This is the only Knowledge website with ‘Picture Points.’</p>
          <p>‘Picture Points’ assists with identifying and most importantly remembering key locations around London.</p>
          <p>When learning The Blue Book it’s very important to call over the ‘Runs’ and Revise ‘Points’ frequently. KnowledgePics gives users that opportunity by having easy access to view PDF ‘Runs’ and Picture Points anytime anywhere via mobile, tablet or computer.</p>
          <p>Just Download and Printout ‘Runs’ when your ready to go out on your moped. (Remember safety gear)</p>
          <p>Learn The Blue Book with KnowledgePics to be always on point</p>

      </div>
      <div class="col-xs-4 soft--top" style="height: 430px;overflow: hidden">
        <a class="twitter-timeline"  href="https://twitter.com/Knowledgepics"  data-widget-id="484102714511466497">Tweets by @Knowledgepics</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
      </div>
    </div>
  </div>
</div>


<?php include 'global/footer.php'; ?>
