<?php include 'global/header.php'; ?>

<?php
include("cms/functions.inc");

$book=($_GET['book']==null)?0:$_GET['book']-1;
$record=mysql_fetch_assoc(getActiveOrder($book, $_SESSION["username"], date('Y-m-d')));

if ($record!=null || $book==0) {

   $listsPerBook = array
  (
  array(1, 4, 36, 53,70),
  array(87,104,121, 138, 155),
  array(172,193, 210,227,245),
  array(264)
  );

?>
<?php include 'global/login-navigation.php'; ?>
<div data-ng-controller="pointsController" class="map" ng-cloak>

    <div class="container container-full soft hard">
        <div class="col-xs-4 hard--right">
            <ul class="nav nav-pills">
                <li class="map-list <?php echo ($_GET['list_id'] == $listsPerBook[$book][0]) ?  'map-list-active' : '' ?>"><a href="runs.php?list_id=<?php echo $listsPerBook[$book][0]?>&book=<?php echo $book+1 ?>" class="hard--bottom map-list--link">List <?php echo 1+($book*5) ?></a></li>
                <li class="map-list <?php echo ($_GET['list_id'] == $listsPerBook[$book][1]) ?  'map-list-active' : '' ?>"><a href="runs.php?list_id=<?php echo $listsPerBook[$book][1]?>&book=<?php echo $book+1 ?>" class="hard--bottom map-list--link">List <?php echo 2+($book*5) ?></a></li>
                <li class="map-list <?php echo ($_GET['list_id'] == $listsPerBook[$book][2]) ?  'map-list-active' : '' ?>"><a href="runs.php?list_id=<?php echo $listsPerBook[$book][2]?>&book=<?php echo $book+1 ?>" class="hard--bottom map-list--link">List <?php echo 3+($book*5) ?></a></li>
                <li class="map-list <?php echo ($_GET['list_id'] == $listsPerBook[$book][3]) ?  'map-list-active' : '' ?>"><a href="runs.php?list_id=<?php echo $listsPerBook[$book][3]?>&book=<?php echo $book+1 ?>" class="hard--bottom map-list--link">List <?php echo 4+($book*5) ?></a></li>
                <li class="map-list <?php echo ($_GET['list_id'] == $listsPerBook[$book][4]) ?  'map-list-active' : '' ?>"><a href="runs.php?list_id=<?php echo $listsPerBook[$book][4]?>&book=<?php echo $book+1 ?>" class="hard--bottom map-list--link">List <?php echo 5+($book*5) ?></a></li>
            </ul>
        </div>
        <div class="col-xs-8"></div>
    </div>

    <div class="container container-full soft hard" >
        <div class="col-xs-4 hard--sides">
          <div class="lighter-gray-background map-navigation">
              <div class="panel-body">
                <ol class="breadcrumb hard">
                  <li><a href="#" class="btn btn-default">Book <?php echo $book+1; ?></a></li>
                  <li><a href="#" class="btn btn-default">List <?php echo (array_search($_GET['list_id'], $listsPerBook[$book])+1+($book*5)) ?></a></li>
                  <li><a href="#" class="btn btn-default">{{activeRun.run}}</a></li>
                </ol>
                <h5 class="text--darker-blue text--norwester flush--top push--bottom">{{activeRun.name}}</h5>

				                <div class="col-xs-12 hard--left soft--bottom">
                    <a href="<?php echo helper::host() ?>/pdf.php?id=<?php echo $_GET['run_id'];?>" class="text--darker-blue" target="_blank">Download {{activeRun.run}} <span class="glyphicon glyphicon-download h3"></span> </a>
                </div>

                <h5 class="text--darker-blue text--norwester push--top col-xs-6 hard--left">Points of interest</h5>
                <h5 class="text--darker-blue text--norwester push--top col-xs-6 text-right hard--right">Open Image</h5>
                <ul class="list list--no-style hard--left">
                  <li data-ng-repeat="point in points" ng-init="$last ? centerMap() : null">
                      <a href="" data-ng-click="openPoint($index,point)" ng-class="{'is-active': selectedItem === $index || selectedItem === point.lng}" class="col-xs-11 hard--left hard--right push-half--bottom"><span class="glyphicon glyphicon-map-marker"></span> {{point.name}}</a>
                      <a href="" data-ng-click="openImage($index,point)" class="glyphicon glyphicon-picture pull-right col-xs-1 hard--left hard--right push-half--bottom"></a></li>
                </ul>
              </div>
          </div>
        </div>

        <leaflet center="london" markers="markers" tiles="tiles" class="col-xs-8" id="map" height="{{height}}">
        </leaflet>


    </div>
    <ul class='hard--left'>
        <li data-ng-repeat="point in points" ng-show="showOverlay && (selectedItem === $index || selectedItem === point.lng)" class="text--center background-overlay image-popup">
            <div class='hard--left'>
                <a class="glyphicon glyphicon-remove text--white image-popup--close" aria-hidden="true" data-ng-click="closeImage()"></a>
                <h3 class="text--white">{{point.name}}</h3>
                <img data-ng-src="{{selectedImage}}" width="800"  />
            </div>
        </li>
    </ul>

</div>



<?php include 'global/footer.php'; ?>

<?php
  }
  else{
    if($book>0)
    $next_page = helper::host()."buy.php?id=".($book+1);
      else
    $next_page = helper::host()."users/login.php";

    header("Location: $next_page");
  }


?>
