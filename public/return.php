<?php
include("cms/functions.inc");

// Init cURL
$request = curl_init();

// Set request options
curl_setopt_array($request, array
(
  CURLOPT_URL => 'https://www.paypal.com/cgi-bin/webscr',
  CURLOPT_POST => TRUE,
  CURLOPT_POSTFIELDS => http_build_query(array
    (
      'cmd' => '_notify-synch',
      'tx' => $_GET['tx'],
      'at' => 'Slm7-mcCh2l4syZuSy6OLm0QdsGjmt9VgNmd5ZXGU3NKQdEgIR0J20JpT3m',
    )),
  CURLOPT_RETURNTRANSFER => TRUE,
  CURLOPT_HEADER => FALSE,
  // CURLOPT_SSL_VERIFYPEER => TRUE,
  // CURLOPT_CAINFO => 'cacert.pem',
));

// Execute request and get response and status code
$response = curl_exec($request);
$status   = curl_getinfo($request, CURLINFO_HTTP_CODE);

// Close connection
curl_close($request);

// Remove SUCCESS part (7 characters long)
$response = substr($response, 7);

// URL decode
$response = urldecode($response);

// Turn into associative array
preg_match_all('/^([^=\s]++)=(.*+)/m', $response, $m, PREG_PATTERN_ORDER);
$response = array_combine($m[1], $m[2]);

// Fix character encoding if different from UTF-8 (in my case)
if(isset($response['charset']) AND strtoupper($response['charset']) !== 'UTF-8')
{
  foreach($response as $key => &$value)
  {
    $value = mb_convert_encoding($value, 'UTF-8', $response['charset']);
  }
  $response['charset_original'] = $response['charset'];
  $response['charset'] = 'UTF-8';
}

	// Sort on keys for readability (handy when debugging)
	ksort($response);

    $title=$response["item_name1"];
	$book=substr($response["item_name1"],strpos($response["item_name1"]," ")+1);
	$book=substr($book,strpos($book," ")-1);

	$response["item_name1"]=substr($response["item_name1"],strpos($response["item_name1"],",")+1);
	
	 $response["item_name1"]=substr( $response["item_name1"], 0, strpos($response["item_name1"],"days")-1);
	 insertOrder($response["payer_email"], date('Y-m-d'), $book-1, $response["item_name1"], $response["mc_gross_1"], $response["txn_id"]);

// use wordwrap() if lines are longer than 70 characters
$msg = wordwrap($msg,70);

 $user=know_user($response["payer_email"]);
	
	if($user['email']==null){
       $code=createRandomKey(4);
       know_users($response["payer_email"],$code, 1, $code, $_SERVER['REMOTE_ADDR'], 0);
	   $message='<p>Dear Member,</p><p>Thank you for registering with KnowledgePics. <p>Your registration details: </p><br>Username:<b> '.$response["payer_email"].'</b><br>Password:<b> '.$code.'</b> <br><br>Be Lucky,<br>The KnowledgePics Team';
	   senduser($response["payer_email"],  $message, 'Registration');

	}


// send email
	  $message='<p>Dear Member,</p><p>Thank you for your order with KnowledgePics. <p>Your Order details: </p><br>'.
		    '<p> <strong>Name:</strong>'.$response["first_name"].' '.$response["last_name"].'</p>'
			.'<p> <strong>Your email:</strong>'.$response["payer_email"].'</p>'
		    .'<p> <strong>Status:</strong>'.$response["payment_status"] .'</p>'
		    .'<p> <strong>Total Price:</strong> £ '.$response["mc_gross"].'</p>'
			.'<br><br>Be Lucky,<br>The KnowledgePics Team';
	
	
	 senduser($response["payer_email"],  $message, 'Order Confirmation');
	 senduser('admin@knowledgepics.com',  $message, 'Order Confirmation');
	 senduser('admin@techliners.com',  $message, 'Order Confirmation');


?>

<?php include 'global/header.php'; ?>

<?php include 'global/navigation.php'; ?>

<div class="light-gray-background">
  <div class="container"><h1>Thank you for your Order for: <?php echo $title; ?>! </h1></div>
</div>

<div class="lighter-gray-background soft--top">
    <div class="container biker-background">

    <div class="row">
      <div class="col-xs-12">
        <h2>Your subscription</h2>
        <div id="result"></div>
        <div class="soft--top">
		    <p> <strong>Name:</strong> <?php echo $response["first_name"].' '.$response["last_name"]; ?></p>
			<p> <strong>Your email:</strong> <?php echo $response["payer_email"]; ?></p>
		    <p> <strong>Status:</strong> <?php echo $response["payment_status"] ?></p>
		    <p> <strong>Payment Date:</strong> <?php echo $response["payment_date"]; ?></p>
		    <p> <strong>Total Price:</strong> &pound <?php echo $response["mc_gross"]; ?></p>

        </div>
    </div>

  </div>
</div>


<?php include 'global/footer.php'; ?>
