<?php include 'global/header.php'; ?>

<?php if (isset($_SESSION["username"]) || ( $_GET['list_id']==1 || $_GET['list_id']==4) ) { ?>
<?php

   $listsPerBook = array
  (
  array(1, 4, 36, 53,70),
  array(87,104,121, 138, 155),
  array(172,193, 210,227,245),
  array(264)
  );

   $book=$_GET['book']-1;

?>

<?php include 'global/login-navigation.php'; ?>
<div data-ng-controller="runsController" class="map" ng-cloak>
    <div class="container container-full soft hard" >
        <div class="col-xs-2 hard--sides"></div>

        <div class="col-xs-8 hard--sides">

          <div class="panel-body">
            <h5 class="text--darker-blue text--norwester push--top">List <?php echo (array_search($_GET['list_id'], $listsPerBook[$book])+1+($book*5)) ?></h5>
            <div class="list-group">
              <ul class="list list--no-style hard--left">
                  <li data-ng-repeat="run in runs">
                      <a href="<?php echo helper::host() ?>map.php?run_id={{run.id}}&list_id=<?php echo $_GET['list_id']?>&book=<?php echo $_GET['book']; ?>" class="list-group-item">{{run.name}}</a>
                  </li>
              </ul>
            </div>
          </div>

        <div class="col-xs-2 hard--sides"></div>
    </div>

</div>



<?php include 'global/footer.php'; ?>

<?php
  }
  else{

    if($_GET['book']==1)
       $next_page = helper::host()."books.php";
    else
       $next_page = helper::host()."users/login.php";

     header("Location: $next_page");
  }


?>
