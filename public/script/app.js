'use strict';

/**
 * @ngdoc overview
 * @name angTestApp
 * @description
 * # angTestApp
 *
 * Main module of the application.
 */


var host = location.host+'/knowledgepics',
//var host = location.host,
    mapUrl = 'http://'+host+'/points_service.php/',
    runsUrl = 'http://'+host+'/view_runs.php/';

function getUrlParam(name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( location.href);
  if( results == null )
    return null;
  else
    return results[1];
}

 var app = angular.module('knowledgeApp', ['leaflet-directive', 'ngResource','ngMessages']);
