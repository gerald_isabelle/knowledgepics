/**
 * pointsController.
 * @param $scope
 * @param $timeout
 * @param leafletData
 * @param run
 * @param point
 */

app.controller('pointsController', [
    '$scope',
    '$timeout',
    'leafletData',
    'run',
    'point',
    function($scope, $timeout, leafletData, run, point){

  var mapOptions = {
    defaultZoom: 13,
    defaultLat: 51.5669466415,
    defaultLng: -0.094358772039413,
    height: 800
  };



  // set up list
  $scope.list = parseInt(getUrlParam("list_id"));
  $scope.runId = parseInt(getUrlParam("run_id"));

  // get runs
  run.get({list_id: $scope.list}).$promise.then(function(response) {
    $scope.runs = response;
    for (var i = 0; i < response.length; i++) {
      if (response[i].id === $scope.runId ) {
        $scope.activeRun = response[i];
        break;
      }
    }
  });

  // get points
  point.get({run_id: $scope.runId}).$promise.then(function(response) {
    $scope.points = response;
    createPoints(response);
  });


  $scope.centerMap = function(){
    $scope.london.lat = $scope.points[0].lang;
    $scope.london.lng = $scope.points[0].lng;
    $timeout(function(){
      $scope.height = parseInt(document.querySelector('.map-navigation').offsetHeight);
    });
  };


  // create Map
  angular.extend($scope, {
      london: {
          lat: mapOptions.defaultLat,
          lng: mapOptions.defaultLng,
          zoom: mapOptions.defaultZoom
      },
      tiles: {
          url: 'http://api.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
          type: 'xyz',
          options: {
              apikey: 'pk.eyJ1IjoidGhla25vd2xlZGdlIiwiYSI6Ilk4WGxtamsifQ.Ov1iGeq_gr_uM1qmFxV0RQ',
              mapid: 'theknowledge.l0605nee'
          }
      },
      height: mapOptions.height
  });


  $scope.openPoint = function(item, point) {
    $scope.selectedItem = item;

    leafletData.getMap().then(function(map) {
      L.marker([point.lang, point.lng]).addTo(map).bindPopup('<h6>'+point.name+'</h6><span><strong>Address: </strong>'+point.address+'</span><br /><span><strong>Postcode: </strong>'+point.post_code+'</span><img src="'+point.image1+'" width="180" class="push--top">').openPopup();
    });

  };

  /* selectedImage image is added on click to avoid, loading all images on page load */
  $scope.openImage = function(item, point) {
    $scope.selectedItem = item;
    $scope.selectedImage = point.image1;
    $scope.showOverlay = true;
  };

  $scope.closeImage = function() {
    $scope.showOverlay = false;
 };


  $scope.sortRuns = function(run) {
    return parseInt(run.name.slice(4,6));
  }

  function createPoints(data){
    var markers = {};

    for(var key in data) {
      if(data[key].name !== undefined) {
        if (!data[key].image) {
          data[key].image = "http://placehold.it/175x150"
        }

        markers[data[key].name] = {
          lat: data[key].lang,
          lng: data[key].lng,
          message: '<h6>' + data[key].name + '</h6><span><strong>Address: </strong>' + data[key].address + '</span><br /><span><strong>Postcode: </strong>' + data[key].post_code + '</span><img src=' + data[key].image1 + ' width="180" class="push--top">',
        };
      }
    }



    angular.extend($scope, {
      markers: markers
    });

  }

  $scope.popupIsOpen = function(item) {
    $scope.selectedItem = item;
  }


  $scope.$on('leafletDirectiveMap.popupopen', function(event, args) {
    for (var key in event.currentScope.markers ) {
      if(event.currentScope.markers[key].lat === args.leafletEvent.popup._latlng.lat &&
         event.currentScope.markers[key].lng === args.leafletEvent.popup._latlng.lng){
           $scope.popupIsOpen(event.currentScope.markers[key].lng)
       }
    }
  });

}]);
