/**
 * pointsController.
 * @param $scope
 * @param run
 */

app.controller('runsController', [
    '$scope',
    'run',
    function($scope, run){

  // set up list
  $scope.list = parseInt(getUrlParam("list_id"));

  // get runs
  run.get({list_id: $scope.list}).$promise.then(function(response) {
    $scope.runs = response;
  });


}]);
