/**
 * points service
 */
app.factory('point', ['$resource', function($resource) {
  return $resource(mapUrl, {}, {
    get: {
      method:'GET',
      isArray:true
    }
  });
}]);