/**
 * runs service
 */
app.factory('run', ['$resource', function($resource) {
  return $resource(runsUrl, {}, {
    get: {
      method:'GET',
      isArray:true
    }
  });
}]);
