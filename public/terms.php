<?php include 'global/header.php'; ?>

<?php include 'global/navigation.php'; ?>

<div class="light-gray-background">
  <div class="container text-center">
    <h1>Terms and Conditions</h1>
  </div>
</div>


<div class="container push--top">
  <div class="col-xs-12">

    <h2>Legal Terms and Conditions of use</h2>

    <p>Your use of this website <b>knowledgepics.com</b>, (named here also as "<b>website</b>") is subject to the terms and conditions as set out here and as may be amended from time to time by the website owners.</p>

    <p>Your use of the www.knowledgepics.com website is confirmation that you have understood and agreed to be bound by all of these Terms and Conditions. Use of this website is prohibited unless you agree to these Terms and Conditions. You must not use this website if you have not read and accepted these terms and conditions.</p>

    <h3>Copyright, Trade Mark and other Intellectual Property Rights</h3>
    <p>The copyright and all other intellectual property rights in this website including all text, graphics, photos, code, files and links belong to <b>knowledgepics.com</b> and the website may not be reproduced, transmitted or stored in whole or in part without <b>knowledgepics.com</b> prior written consent. However, you may print out, save or download individual selections for your own personal, private and non-commercial use.</p>

    <p>The <b>knowledgepics.com</b> logo is a registered trademark. You may not use or reproduce this trademark in any way or form.</p>

    <h3>Book Prices </h3>

    <p>All prices displayed on <b>knowledgepics.com</b> are subject to contract.</p>

    <h3>My Account Service</h3>
    <p>The email subscription in  <b>knowledgepics.com</b> is offered to the public as an added service for their own benefit. Users of  <b>knowledgepics.com</b> may at any
    time discontinue the service by accessing the appropriate section of the site to cancel their email subscription <b>knowledgepics.com</b> does not guarantee continuity of the service.</p>

    <p>Details entered into  <b>knowledgepics.com</b> will not only be forwarded to the relevant franchise office for the area/s of you choice and will not be forwarded on to third parties outside the  <b>knowledgepics.com</b>
    unless you have given your specific consent.</p>

    <p>The security of your <b>knowledgepics.com</b> password cannot be guaranteed.</p>

    <p>Links to this website and links to third party websites</p>

    <p>The website may contain links to websites operated by third parties. Such links are provided for your convenience only and we have no control over their individual content.  <b>knowledgepics.com</b> therefore makes no warranties or representations as to the accuracy or completeness of any of the information appearing in relation to any linked websites nor as to the suitability or quality of any of their products or services.</p>

    <p>Links to this website may not be included in any other website without prior written consent of  <b>knowledgepics.com</b>.</p>

    <h3>Third Party Services </h3>

    <p style="text-align: justify;">
    Where details of third party services are included in the website, they are given for your information only.  <b>knowledgepics.com</b> do not recommend or guarantee the quality of service or products that you receive from any third party service.
    </p>

    <h3>Privacy Policy</h3>

    <p>
    We reserve the right to change the content of this website (including the terms and conditions) at any time. Therefore, it is your responsibility to visit this page of this site on a regular basis to ascertain whether any amendments have been made. If you do not agree to amendments made, you should immediately cease to use this site.

    Whilst we take every care to ensure that the standard of this website remains high and to maintain the continuity of it, we do not accept any ongoing obligation or responsibility to operate this website (or any particular part of it). If any part of our Terms and Conditions is deemed to be unenforceable (including any provision in which we exclude our liability to you) the enforceability of any other part of these conditions will not be affected.
    </p>

    <h3>IP Addresses</h3>

    <p>
     <b>knowledgepics.com</b> logs IP addresses to help diagnose problems with our servers and for system administration.  <b>knowledgepics.com</b> shall only use these addresses for technical & tracking purposes.
    </p>

    <h3>Log Files</h3>

    <p>
     <b>knowledgepics.com</b> captures industry standard information solely for the purposes of analysis. Information contained in log files is intended for internal use only and will not be disclosed to our partners.
    </p>

    <h3>Jurisdiction</h3>
    <p>
    These terms and conditions and any claim based upon use of this website shall be governed by the laws of England and you agree to submit the exclusive jurisdiction of the English courts.
    </p>

    <h3>How to contact knowledgepics.com</h3>

    <p>
    We welcome your views about our website and its content. If you would like to contact us with any queries or comments please send an email to: info@knowledgepics.com.
    </p>
    </div>
</div>


<?php include 'global/footer.php'; ?>
