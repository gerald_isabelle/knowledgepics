<?php
session_start();
include("know_users.inc");

/******* USERS ********/

/*

email: the username
password: set by the user
flag: 0 or 1, depending whether you wish the user to be active or not
confirm: whether has been confirmed or not
code: the code
IP: register ip address

*/

if (@$_POST["userAction"] == "registerUser")
{
    session_start();
	$code=createRandomKey(8);

	$user=checkknow_user(@$_POST["email"],@$_POST["password"]);

	if($user['email']==null){

		know_users(@$_POST["email"],@$_POST["password"], 0, $code, $_SERVER['REMOTE_ADDR'], 0);
		sendcontact('Knowledge Pics: Confirmation Request', '<p>Dear Member,</p><p>Thank you for registering with KnowledgePics. Please confirm details first by clicking on link below.</p><a href="http://knowledgepics.com/users/confirm.php?code='.$code.'">http://knowledgepics.com/users/confirm.php?code='.$code.'</a><p>If unable to click link please copy and paste in browser. You can now access the Map section of List1Run1 with Username and Password.</p><p>You will be notified via email when all 320 Runs including Picture Points are available to view and download.</p><br>Username:<b> '.@$_POST["email"].'</b><br>Password:<b> '.@$_POST["password"].'</b> <br><br>Be Lucky,<br>The KnowledgePics Team
      ', @$_POST["email"]);
    sendcontact('Knowledge Pics: User Sign up', 'User <b>'.@$_POST["email"].'</b> signed up', 'admin@knowledgepics.com');
    $next_page = "register.php?msg=1";
    header("Location: $next_page");
  }else{
    $next_page = "register.php";
    header("Location: $next_page");
  }

}


if (@$_POST["userAction"] == "bon_update_user")
{
    uknow_users ($_POST["email"], $_POST["password"], $_POST["flag"]);
    $next_page = "update_user.php?id=".$_POST["email"];
    header("Location: $next_page");

}


if (@$_POST["userAction"] == "forgot")
{
    session_start();

	$user=know_user(@$_POST["email"]);

	if($user['email']){

		$_SESSION["email"]=$user['email'];
		$code=createRandomKey(8);
    	forgot_pwd(@$_POST["email"], $code, $_SERVER['REMOTE_ADDR']);
		sendcontact('KnowledgePics: Reset Password Request', '<p>Dear Member,</p> <p>Username:<b> '.@$_POST["email"].'</b></p>  <p>Please reset your password here: <a href="http://knowledgepics.com/users/reset.php?code='.$code.'">http://knowledgepics.com/users/reset.php?code='.$code.'</a></p> <p> <br><br>Be Lucky,<br>The KnowledgePics Team', @$_POST["email"]);
    }

	$next_page = "forgot.php?msg=1";
    header("Location: $next_page");

}

if (@$_POST["userAction"] == "remind")
{
    session_start();

	$user=know_user(@$_POST["email"]);

	if($user['email']){

		$_SESSION["email"]=$user['email'];
		sendcontact('KnowledgePics: Password Reminder', '<p>Dear Member</p> <p>username:<b> '.@$_POST["email"].'</b></p>  <p>Your password is: '.$user['password'].'  </p> <br><br>Be Lucky,<br>The KnowledgePics Team', @$_POST["email"]);
    }

	$next_page = "remind.php?msg=1";
    header("Location: $next_page");

}

if (@$_POST["userAction"] == "password")
{
    if(!$_POST["password"]){
      $next_page = "password.php";
      header("Location: $next_page");
    }else if($_POST["password"] === $_POST["newpassword"]) {
      upwd($_SESSION["username"],@$_POST["password"]);
      //sendcontact('KnowledgePics: Reset Password', '<p>Dear User</p> <p>username:<b> '.$_SESSION["email"].'</b></p>  <p>Your new password is:'.$_POST["password"].'  </p> <p> Best Wishes, </p> <br><br>Be Lucky,<br>The KnowledgePics Team', @$_SESSION["email"]);
      $next_page = "password.php?msg=1";
      header("Location: $next_page");
    }else{
      $next_page = "password.php?msg=2";
      header("Location: $next_page");
    }

}

if (@$_POST["userAction"] == "updateUser")
{

    updatesubscribers(@$_POST['name'], @$_POST['surname'], @$_POST['sex'], @$_POST['DateOfBirth'], @$_POST['currentlyStudying'], @$_POST['level'], @$_POST['badge'], @$_POST['mobileNumber'], @$_POST['hearAbout'], @$_POST['readStatus'], @$_POST['papers'], @$_POST['school'], @$_POST['contactMethod'], @$_POST['email'] );
    header("Location: profile.php");
}

?>
