<?php include '../global/header.php'; ?>

<?php include '../global/navigation.php'; ?>

<div class="light-gray-background">
	<div class="container"><h1>Please log in</h1></div>
</div>

<?php      $message = "Login failed. Please check your username and password and try again.<br>"; ?>

<div class="lighter-gray-background soft--top">
	<div class="container soft--ends biker-background">
		<div class="row">
			<div class="col-xs-4"></div>
			<div class="col-xs-4">
				<div align='center'><font color='red'><?php if($_GET['msg']==1)  echo $message; ?></font></div>


					<form method="POST" class="form-signin" role="form"  action="<?php echo $_SERVER['PHP_SELF']?>">
						<input TYPE="hidden" NAME="userAction" VALUE="loginUser">
						<input type="text" name="username" value="" class="form-control" placeholder="Email address" required autofocus>
						<input type="password" class="form-control" placeholder="Password"  name="password" required>
						<input type="submit" value="Submit" name="Button" class="btn btn-lg btn-primary btn-block">
						<div class="well well-sm push--ends">
							<a href="forgot.php"><span class="glyphicon glyphicon-exclamation-sign"></span> Password reminder</a>
						</div>

					</form>
				</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
</div>
<?php include '../global/footer.php'; ?>
