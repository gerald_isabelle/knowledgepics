<?php include '../global/header.php'; ?>

<?php include '../global/login-navigation.php'; ?>
<?php include("../cms/functions.inc"); ?>
<div class="light-gray-background">
    <div class="container"><h3> <?php echo $_SESSION["username"]; ?></h3></div>
</div>

<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

tr:hover{background-color:#f5f5f5}
</style>


<div class="lighter-gray-background soft">
    <div class="container soft--ends">
        <div class="row">
            <div class="col-xs-3">
                <ul class="list-unstyled list-tab">
                    <li><a href="profile.php" class="light-gray-background text--center col-xs-12 push-half--bottom">my Profile</a></li>
                    <li><a href="mybooks.php" class="light-gray-background active text--center col-xs-12 push-half--bottom">my Books</a></li>
                </ul>

            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-8">
				
                <ul class="list-unstyled">
                    <li class="soft white-background">
					                        <div class="row">
                            <div class="col-xs-6"><h3 class="flush--top">Your Subscriptions</h3></div>
                        </div>

						<?php 
						   $results=getOrders($_SESSION["username"]);
						?>
						<table>
  <tr>
	<th> Email </th>
    <th> Subscription </th>
    <th> End Date</th>
    <th> Book </th>
	<th> Price </th>
  </tr>
  <?php
		while($result=mysql_fetch_assoc($results)){
  ?>  
  <tr>
    <td><?php echo $result['email']?></td>
	<td><?php echo date('d-m-Y', strtotime($result['startDate'])); ?></td>
    <td><?php echo date('d-m-Y', strtotime($result['endDate'])); ?></td>
    <td><?php echo ($result['book']+1)?></td>
    <td>&pound<?php echo $result['price']?></td>
  </tr>
  <? } ?>
</table>

                    </li>
                    <li class=""></li>
                    <li class=""></li>
                </ul>
            </div>


        </div>
    </div>
</div>


<?php include '../global/footer.php'; ?>
