<?php include '../global/header.php'; ?>

<?php include '../global/navigation.php'; ?>

<div class="light-gray-background">
	<div class="container"><h1>reset password</h1></div>
</div>

<div class="lighter-gray-background soft--top">
	<div class="container soft--ends biker-background">
		<div class="row">
			<div class="col-xs-4"></div>
			<div class="col-xs-4">

				<div align='center'><font color='red'><?php
					if( isset($_GET['msg']) && ($_GET['msg']) == '1') {
						$message = 'Your password have been reset.';
					} elseif (($_GET['msg']) == '2') {
						$message = 'Please make sure the 2 password fields match';
					}
					echo @$message;
					?>
					</font></div>


					<form method="POST" action="form.php" class="form-signin" role="form">
							<input TYPE="hidden" NAME="userAction" VALUE="password">
							<input type="password" name="password" class="form-control" placeholder="new Password" required autofocus>
							<input type="password" name="newpassword" class="form-control" placeholder="Retype new Password" required autofocus>
							<input type="submit" value="Submit" name="Button" class="btn btn-lg btn-primary btn-block">

					</form>
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
</div>

<?php include '../global/footer.php'; ?>
