<?php include '../global/header.php'; ?>

<?php include '../global/login-navigation.php'; ?>
<?php include("know_users.inc"); ?>
<?php 
    $results=getsubscribers($_SESSION["username"]);
    $result=mysql_fetch_assoc($results)
?>
<div class="light-gray-background">
    <div class="container"><h3> <?php echo $_SESSION["username"]; ?></h3></div>
</div>

<div class="lighter-gray-background soft">
    <div class="container soft--ends">
        <div class="row">
            <div class="col-xs-3">
                <ul class="list-unstyled list-tab">
                    <li><a href="profile.php" class="light-gray-background active text--center col-xs-12 push-half--bottom">my Profile</a></li>
                    <li><a href="mybooks.php" class="light-gray-background text--center col-xs-12 push-half--bottom">my Books</a></li>
                </ul>

            </div>
            <div class="col-xs-1"></div>
            <div class="col-xs-8">
                <ul class="list-unstyled">
                    <li class="soft white-background">
                        <div class="row">
                            <div class="col-xs-6"><h3 class="flush--top"><?php echo $result['name'].' '.$result['surname']?></h3></div>
                            <div class="col-xs-6 text--right"></div>
                        </div>
                        <form method="POST" class="form-signin" role="form" action="form.php">
                            <input TYPE="hidden" NAME="userAction" VALUE="updateUser">
                            <input type="text" name="name" value="<?php echo $result['name'] ?>" class="form-control" placeholder="First name" autofocus>
                            <input type="text" name="surname" value="<?php echo $result['surname'] ?>" class="form-control" placeholder="Last name" autofocus>
                            <select class="form-control" name="sex">
                                <option value="">Sex</option>
                                <option value="Male" <?php if ($result['sex'] == 'Male' ) echo 'selected'; ?> >Male</option>
                                <option value="Female" <?php if ($result['sex'] == 'Female' ) echo 'selected'; ?>>Female</option>
                            </select>
                            <input type="text" name="DateOfBirth" value="<?php echo date('d-m-Y', strtotime($result['DateOfBirth'])); ?>" class="form-control" placeholder="Date of birth e.g dd/mm/yyyy" autofocus>
                            <input type="text" name="email" value="<?php echo $_SESSION["username"]; ?>" class="form-control" placeholder="Email address" required autofocus>
                            <input type="text" name="mobileNumber" value="<?php echo $result['mobileNumber'] ?>" class="form-control" placeholder="Mobile number" autofocus>
                            <select class="form-control" name="hearAbout">
                                <option value="">Where did you hear about us ?</option>
                                <option value="Twitter" <?php if ($result['hearAbout'] == 'Twitter' ) echo 'selected'; ?>>Twitter</option>
                                <option value="Youtube" <?php if ($result['hearAbout'] == 'Youtube' ) echo 'selected'; ?>>Youtube</option>
                                <option value="Facebook" <?php if ($result['hearAbout'] == 'Facebook' ) echo 'selected'; ?>>Facebook</option>
                                <option value="Google" <?php if ($result['hearAbout'] == 'Google' ) echo 'selected'; ?>>Google</option>
                                <option value="Friend" <?php if ($result['hearAbout'] == 'Friend' ) echo 'selected'; ?>>Friend</option>
                                <option value="Taxi Driver" <?php if ($result['hearAbout'] == 'Taxi Driver' ) echo 'selected'; ?>>Taxi Driver</option>
                                <option value="Knowledge Student" <?php if ($result['hearAbout'] == 'Knowledge Student"' ) echo 'selected'; ?>>Knowledge Student</option>
                                <option value="Somewhere else" <?php if ($result['hearAbout'] == 'Somewhere else' ) echo 'selected'; ?>>Somewhere else</option>
                            </select>
                            <input type="submit" value="Update" name="Button" class="btn btn-lg btn-primary btn-block">
                        </form>
                    </li>
                    <li class=""></li>
                    <li class=""></li>
                </ul>
            </div>


        </div>
    </div>
</div>


<?php include '../global/footer.php'; ?>
