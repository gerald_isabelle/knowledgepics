<?php include '../global/header.php'; ?>

<?php include '../global/navigation.php'; ?>

<div class="light-gray-background">
	<div class="container"><h1>Please register</h1></div>
</div>

<div class="lighter-gray-background soft--top">
	<div class="container soft--ends biker-background">
		<div class="row">
			<div class="col-xs-4"></div>
			<div class="col-xs-4">


				<div align='center'>
					<font color='red'>
					<?php
						if( isset($_GET['msg']) && ($_GET['msg']) == '1'){
							$message = 'Thank you very much for
			registering, please check your email and click on the email
			confirmation link to active your profile.';
						}
						echo @$message;
					?>
					</font>
				</div>


				<form method="POST" class="form-signin" role="form" action="form.php">
					<input type="text" name="email" value="" class="form-control" placeholder="Email address" required autofocus>
					<input type="password" name="password" class="form-control" placeholder="Password" required>
					<input TYPE="hidden" NAME="userAction" VALUE="registerUser">
					<input type="submit" value="Submit" name="Button" class="btn btn-lg btn-primary btn-block">
				</form>
			</div>
			<div class="col-xs-4"></div>
		</div>
	</div>
</div>

<?php include '../global/footer.php'; ?>
