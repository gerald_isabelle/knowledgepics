<?php include 'global/header.php'; ?>

<?php include 'global/navigation.php'; ?>

<div class="container push--top" style="height:500px;">
  <div class="row text--center">
    <video id="video-intro" height="240" controls>
      <source src="<?php echo helper::host() ?>video/londontaxi.webm" type="video/webm">
      <source src="<?php echo helper::host() ?>video/londontaxi.mp4" type="video/mp4"
    </video>
  </div>
</div>


<?php include 'global/footer.php'; ?>
